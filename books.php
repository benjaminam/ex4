<?php
    include "db.php";
    include "query.php";
?>
<html>
    <head>
    <title>Books</title>
    </head>
    <body>
        <p>
        <?php
            $db = new DB('localhost','intro','root','');
            $dbc = $db->connect();
            $query = new Query($dbc);
            $q = "SELECT books.id,books.author,books.title,users.name 
            FROM books JOIN users 
            ON books.user_id = users.id";
            $result = $query->query($q);
            echo '<br>';
            //$result->num_rows;
            if($result->num_rows > 0){
                echo '<table style="width:25%;">';
                echo '<tr><th style="border-style: solid;">Title</th style="border-style: solid";><th style="border-style: solid;">Reader name</th></tr>';
                while($row = $result->fetch_assoc()){
                    echo '<tr>';
                    echo '<td style="border-style: solid";>' .$row['title'].'</td><td style=" border-style: solid;">' .$row['name'].'</td>';
                    echo '</tr>';
                }
                echo '</table>';
            }
            else {
                echo "Sorry no results";
            }
        ?>
        </p>
    </body>
</html>